import pyotb
import time

start = time.time()
pxs = pyotb.BundleToPerfectSensor(inp='pan.vrt', inxs='xs.tif', ram=190000) 
pxs.write('pxs_pyotb.tif', pixel_type='uint16')

print('ELAPSED TIME:', time.time() - start)
